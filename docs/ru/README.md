# Сканирующая ракета с радарами

Хотите прочитать на другом языке? | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Быстрые ссылки

[Список изменений](CHANGELOG.md) | [Руководство контрибьютора](CONTRIBUTING.md)
|---|---|

## Содержание

* [Введение](#overview)
* [Настройки мода](#mod-settings)
    * [Для карты](#map)
* [Будущие планы](#future-plans)
* [Сообщить об ошибки](#issue)
* [Запросить функцию](#feature)
* [Установка](#installing)
* [Зависимости](#dependencies)
    * [Необходимые](#required)
* [Лицензия](#license)

## <a name="overview"></a> Введение

Сканирует поверхность после запуска ракеты с радарами.

![gif](https://i.imgur.com/PcSgbtQ.gif)

## <a name="mod-settings"></a> Настройки мода

### <a name="map"></a> Для карт:

| Описание | Параметры | (По умолчанию) |
| -------- | --------- | -------------- |
| Сканирует поверхность после запуска ракеты с радарами | 1-100 | 18 |

## <a name="future-plans"></a> Будущие планы

* Выбор места сканирования после запуска или перед запуском. (Скорее всего, отдельный мод)

## <a name="issue"></a> Нашли ошибку?

Пожалуйста, сообщайте о любых проблемах или ошибках в документации, вы можете помочь нам
[submitting an issue][issues] на нашем GitLab репозитории или сообщите на [mods.factorio.com][mod portal] или на [forums.factorio.com][homepage].

## <a name="feature"></a> Хотите новую функцию?

Вы можете *запросить* новую функцию [submitting an issue][issues] на нашем GitLab репозитории или сообщите на [mods.factorio.com][mod portal] или на [forums.factorio.com][homepage].

## <a name="installing"></a> Установка

Если вы скачали zip архив:

* просто поместите его в директорию модов.

Для большей информации, смотрите [вики Factorio "загрузка и установка модов"](https://wiki.factorio.com/Modding/ru#.D0.97.D0.B0.D0.B3.D1.80.D1.83.D0.B7.D0.BA.D0.B0_.D0.B8_.D1.83.D1.81.D1.82.D0.B0.D0.BD.D0.BE.D0.B2.D0.BA.D0.B0_.D0.BC.D0.BE.D0.B4.D0.BE.D0.B2).

Если вы скачали исходный архив (GitLab):

* скопируйте данный мод в директорию модов Factorio
* переименуйте данный мод в scan-rocket-with-radars_*версия*, где *версия* это версия мода, которую вы скачали (например, 0.5.1)

## <a name="dependencies"></a> Зависимости

### <a name="required"></a> Необходимые

* [Event listener](https://mods.factorio.com/mod/event-listener)

## <a name="license"></a> Лицензия

Этот проект защищен авторским правом © 2017, 2019 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Использование исходного кода, включенного здесь, регламентируется лицензией MIT (The MIT License). Смотрите [LICENCE](/LICENCE) файл для разбора.

[issues]: https://gitlab.com/ZwerOxotnik/scan-rocket-with-radars/issues
[mod portal]: https://mods.factorio.com/mod/scan-rocket-with-radars/discussion
[homepage]: https://forums.factorio.com/viewtopic.php?f=190&t=64621
[Factorio]: https://factorio.com/
