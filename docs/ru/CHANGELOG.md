# Список изменений

## 2019-03-05

### [v0.5.2][v0.5.2]

* Изменен стиль для названия мода

### [v0.5.1][v0.5.1]

* Другие зависимости

## 2019-02-28

### [v0.5.0][v0.5.0]

* Обновлена локализация
* Обовлён встроенный модуль «Event listener» до 0.3.0

## 2019-02-27

### [v0.4.0][v0.4.0]

* Обновлён для Factorio 0.17
* Обновлена локализация
* Добавлен модуль "Event listener"

## 2017-12-14

### [v0.3.0][v0.3.0]

* Обновлён для Factorio 0.16

### [v0.2.0][v0.2.0]

* Обновлён для Factorio 0.15
* Добавлен радиус сканирования в настройках мода

### [v0.1.0][v0.1.0]

* Первый выпуск для 0.16

[v0.5.2]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.5.2.zip
[v0.5.1]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.5.1.zip
[v0.5.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.5.0.zip
[v0.4.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.4.0.zip
[v0.3.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.3.0.zip
[v0.2.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.2.0.zip
[v0.1.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.1.0.zip
