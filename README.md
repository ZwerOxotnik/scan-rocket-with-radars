# Scanning rocket with radars

Read this in another language | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Quick Links

[Changelog](CHANGELOG.md) | [Contributing](CONTRIBUTING.md)
|---|---|

## Contents

* [Overview](#overview)
* [Mod settings](#mod-settings)
    * [For maps](#for-maps)
* [Future plans](#future-plans)
* [Issues](#issue)
* [Features](#feature)
* [Installing](#installing)
* [Dependencies](#dependencies)
    * [Required](#required)
* [License](#license)

## Overview

Scans surface after launching a missile with radars

![gif](https://i.imgur.com/PcSgbtQ.gif)

## <a name="mod-settings"></a> Mod settings

### <a name="for-maps"></a> For maps

| Description | Parameters | (Default) |
| ----------- | ---------- | --------- |
| Scan radius | 1-100 | 18 |

## <a name="future-plans"></a> Future plans

* Selecting a scan location after launch or before launch. (Most likely a separate mod)

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by [submitting an issue][issues] to our GitLab Repository or on [mods.factorio.com][mod portal] or on [forums.factorio.com][homepage].

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by [submitting an issue][issues] to our GitLab Repository or on [mods.factorio.com][mod portal] or on [forums.factorio.com][homepage].

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

If you have downloaded the source archive (GitLab):

* copy the mod directory into your factorio mods directory
* rename the mod directory to scan-rocket-with-radars_*versionnumber*, where *versionnumber* is the version of the mod that you've downloaded (e.g., 0.5.1)

## Dependencies

### Required

* [Event listener](https://mods.factorio.com/mod/event-listener)

## License

This project is copyright © 2017, 2019 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Use of the source code included here is governed by the MIT licence. See the [LICENCE](/LICENCE) file for details.

[issues]: https://gitlab.com/ZwerOxotnik/scan-rocket-with-radars/issues
[mod portal]: https://mods.factorio.com/mod/scan-rocket-with-radars/discussion
[homepage]: https://forums.factorio.com/viewtopic.php?f=190&t=64621
[Factorio]: https://factorio.com/
