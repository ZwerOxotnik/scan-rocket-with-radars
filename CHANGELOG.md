# Changelog

## 2019-03-05

### [v0.5.2][v0.5.2]

* Changed style for name of the mod

### [v0.5.1][v0.5.1]

* Another dependencies

## 2019-02-28

### [v0.5.0][v0.5.0]

* Updated localization
* Updated embedded module «Event listener» to 0.3.0

## 2019-02-27

### [v0.4.0][v0.4.0]

* Updated for Factorio 0.17
* Updated localization
* Added module "Event listener"

## 2017-12-14

### [v0.3.0][v0.3.0]

* Updated for Factorio 0.16

### [v0.2.0][v0.2.0]

* Updated for Factorio 0.15
* Added radius in the mod settings

### [v0.1.0][v0.1.0]

* First release for 0.14

[v0.5.2]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.5.2.zip
[v0.5.1]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.5.1.zip
[v0.5.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.5.0.zip
[v0.4.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.4.0.zip
[v0.3.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.3.0.zip
[v0.2.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.2.0.zip
[v0.1.0]: https://mods.factorio.com/api/downloads/data/mods/2279/scan-rocket-with-radars_0.1.0.zip
