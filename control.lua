-- Copyright (c) 2017, 2019 ZwerOxotnik <zweroxotnik@gmail.com>
-- Licensed under the MIT licence;

event_listener = require("__event-listener__/branch-2/stable-version")
local modules = {}
modules.scan_rocket_with_radars = require('scan-rocket-with-radars/control')

event_listener.add_events(modules)
